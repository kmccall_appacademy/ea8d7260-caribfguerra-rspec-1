# def translate(string)
#   characters = word.chars
#   characters.rotate! until 'aeiou'.include?(characters[0])
#   (characters << "ay").join
# end

def my_chars(string)
  chars = string.chars
  chars.map!.with_index do |c, i|
    if c == "q" && chars[i+1] == "u"
      c += chars[i+1]
    else
      c = c unless c == "u"
    end
  end

  chars.compact
end

def translate(string)
  words = string.split
  translated = []

  words.each do |word|
    characters = my_chars(word)
    characters.rotate! until 'aeiou'.include?(characters[0])
    translated << characters.join + "ay"
 end
 
 translated.join(" ")
end
