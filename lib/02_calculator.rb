def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  numbers.empty? ? 0 : numbers.reduce(:+)
end

def multiply(num1, num2)
  num1 * num2
end

def power(number, exponent)
  number ** exponent
end

def factorial(number)
  (1..number).reduce(:+)
end
