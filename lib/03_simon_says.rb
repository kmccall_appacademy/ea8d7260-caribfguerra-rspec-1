require 'byebug'
def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, times=2)
  ("#{word} " * times).rstrip
end

def start_of_word(word, number=1)
  word.slice(0...number)
end

def first_word(words)
  words.split.first
end

def titleize(title)
  words = title.capitalize!.split

  words.map.with_index do |word, index|
    little_words = ["over", "the", "and"]
    
    if !little_words.include?(word)
      word.capitalize
    else
      word
    end
  end.join(" ")
end
